import React from 'react';
import { Navigate, Route, HashRouter as Router, Routes } from 'react-router-dom';
import './App.css';
import { ProjectForm } from './ProjectForm';
import { ProjectList } from './ProjectList';

const App: React.FC = () => {
  return (
    <Router>
      <Routes>
        <Route path='/projects/new' element={<ProjectForm />} />
        <Route path='/projects/:projectId' element={<ProjectForm />} />
        <Route path='/projects' element={<ProjectList />} />
        <Route path="*" element={<Navigate to='/projects/new' />} /* Fallback route */ />
      </Routes>
    </Router>
  )
}

export default App;
