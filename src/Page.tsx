import React from 'react'
import { Navbar } from './Navbar'
import type { NavbarLink } from './types'

type PageProps = {
  links?: NavbarLink[];
  currentPageName: string;
  toolbar?: JSX.Element;
  children?: React.ReactNode
}

export const Page: React.FC<PageProps> = ({ children, toolbar, links = [], currentPageName }) => {
  return <>
    <Navbar links={links} currentPageName={currentPageName} toolbar={toolbar} />
    <main style={{ padding: 20 }}>
      {children}
    </main>
  </>
}