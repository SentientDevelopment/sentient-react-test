import React, { useCallback, useContext, useState } from 'react'
import type { Form } from './types'

type DataStorage = {
  saveForm(data: Form): Promise<void>;
  loadForm(id: number): Promise<Form | undefined>
  loadForms(): Promise<Form[]>
  deleteForm(id: number): Promise<void>
}
type DataPersistenceContextProviderProps = {
  children?: React.ReactNode
}
const dataPersistenceContext = React.createContext<DataStorage | undefined>(undefined);

export const DataPersistenceContextProvider: React.FC<DataPersistenceContextProviderProps> = ({ children }) => {
  const [data, setData] = useState<ReadonlyArray<Form>>([])
  const [nextId, setNextId] = useState<number>(1);

  const save = (form: Form) => {
    setData(currentData => {
      if (!form.id) {
        form.id = nextId;
        setNextId(nextId + 1)
      }
      const currentIndex = currentData.findIndex(d => d.id === form.id)
      if (currentIndex !== undefined) {
        return [
          ...data.slice(0, currentIndex),
          form,
          ...data.slice(currentIndex + 1, data.length)
        ]
      }
      else return [...data, form]
    })
  }

  const saveForm = async (data: Form) => {
    return new Promise<void>((resolve) => {
      setTimeout(() => {
        save(data)
        resolve()
      }, 500)
    })
  }

  const loadForm = async (id: number) => {
    return new Promise<Form | undefined>((resolve) => {
      setTimeout(() => resolve(data.find(form => form.id === id)), 500)
    });
  };

  const loadForms = async () => {
    return new Promise<Form[]>((resolve) => {
      setTimeout(() => resolve(data as Form[]), 500)
    });
  };

  const deleteForm = (id: number) =>
    new Promise<void>((resolve) => {
      setTimeout(() => {
        setData(currentData => currentData.filter(form => form.id !== id))
        resolve()
      }, 500)
    })

  return <dataPersistenceContext.Provider value={{
    loadForm,
    loadForms,
    saveForm,
    deleteForm
  }}>{children}</dataPersistenceContext.Provider>
};

export function useDataPersistence() {
  const dataPersistence = useContext(dataPersistenceContext)
  if (!dataPersistence
    || !dataPersistence?.deleteForm
    || !dataPersistence?.loadForm
    || !dataPersistence?.loadForms
    || !dataPersistence?.saveForm)
    throw new Error('Data persistence must be used from within a DataPersistenceContextProvider!')
  return dataPersistence;
}