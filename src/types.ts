export type Form = {
  id: number;
}

export type NavbarLink = {
  text: string;
  path: string;
}