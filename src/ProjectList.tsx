import { Button } from '@mui/material'
import React from 'react'
import { Page } from './Page'

type ListProps = {}
export const ProjectList: React.FC<ListProps> = () => {
  return (
    <Page
      toolbar={<>
        <Button color='inherit' href='/#/project/new'>New Project</Button>
      </>}
      currentPageName='All Projects'
    >
      <ul>
        <li>Todo</li>
      </ul>
    </Page>
  )
}
