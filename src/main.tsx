import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { CssBaseline } from '@mui/material'
import { DataPersistenceContextProvider } from './DataPersistenceContext'
import './index.css';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <DataPersistenceContextProvider>
      <CssBaseline />
      <App />
    </DataPersistenceContextProvider>
  </React.StrictMode>,
)