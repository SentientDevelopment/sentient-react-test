import React, { useState } from 'react';
import { Button, FormControl, FormLabel, Input } from '@mui/material'
import './App.css';
import { Page } from './Page';
import type { Form } from './types';

export const ProjectForm: React.FC = () => {
  const [formState, setFormState] = useState<Partial<Form>>({})

  return (
    <Page
      toolbar={<>
        <Button color='inherit' href='/#/project/new'>New Project</Button>
        <Button color='inherit'>Save Project</Button>
      </>}
      links={[{ path: '/projects', text: 'projects' }]} currentPageName={'Unnamed Project'}>
      <FormControl>
        <FormLabel htmlFor='name'>Project Name</FormLabel>
        <Input id='name' value={''} />
      </FormControl>
    </Page>
  )
}
