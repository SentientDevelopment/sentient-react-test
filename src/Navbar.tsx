import { AppBar, Breadcrumbs, Link, Toolbar, Typography } from '@mui/material'
import React from 'react'
import type { NavbarLink } from './types'

type NavbarProps = {
  links: NavbarLink[],
  currentPageName: string;
  toolbar?: JSX.Element
}
export const Navbar: React.FC<NavbarProps> = ({ links, currentPageName, toolbar }) => {
  return (
    <AppBar position="static">
      <Toolbar style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Breadcrumbs style={{ color: '#c8c8f5' }} aria-label="breadcrumb">
          {links.map(link => (
            <Link key={link.path} color="inherit" href={'/#' + link.path}>
              {link.text}
            </Link>
          ))}
          <Typography style={{ color: 'white' }}>{currentPageName}</Typography>
        </Breadcrumbs>
        <span>{toolbar}</span>
      </Toolbar>
    </AppBar>
  )
}