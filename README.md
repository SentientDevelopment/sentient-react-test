# Sentient React Typescript Test

## Overview

We will create a small application that lets us create and edit 'Projects', then view them in a list and delete them. You do not have to do the instructions in order. We are using material-ui for the components in this app. You do not have to use components from there, but feel free to make use of the material-ui library.

## Time Limit
Please spend 2 hours max on this exercise, you will not be judged on how much you get done and as coders we always feel that there is more to do. The aim of this exercise is just to get some code you have written for a technical interview.


### Routes

`/#/projects` is the project list

`/#/projects/new` creates a new project

`/#/projects/:id` displays an existing project

## Before Getting Started
1. install the latest version of node.js (min version v16)
## Getting Started

1. `yarn` install required packages
2. `yarn dev` start program in dev mode

## Troubleshooting
Ensure you are using Typescript version from this workspace (TS 5+) 

## Submission
Either:
Upload to a private github repository and add _SentientSoftware-IT_ (email: it@sentientsoftware.com) as a collaborator (read-access only is fine), or

Upload to a private bitbucket repository and add _it@sentientppm.com_ as a user with read access

## Instructions

1. [types.ts] Alter the Form type to include a required 'name' property
2. [Project Form] Save the project name from the input into `formState` on name input change
3. [Project Form] Alter the Page component attributes to display the project's name in the breadcrumb at the top of the page if it exists
4. [Project Form] Add two new fields to the form: Description (multi-line textbox) and total budget (number).
5. [Project Form] Save the Form to the ✌database✌ by using the custom hook `useDataPersistence`. This provides the loadForm, loadForms, saveForm and deleteForm functions. You should use this throughout the app to save and fetch data.
6. [Project List] Show a list of the created forms. Each item should display the project ID, name and budget.
7. [Project List] Navigate to the Project form for a project when a list item is clicked
8. [Project Form] Load a saved form when you navigate to `/#/projects/:id`. If the form does not exist, do not display the regular form. Instead, display an [Alert](https://mui.com/material-ui/react-alert/) with an appropriate message and a link back to the project list page.
9. [Project List] Delete an item in the project list
10. [Project Form] Write some validation to ensure the project cannot be saved if it has no name or description
11. Handle the loading/saving logic gracefully with some sort of indication to the user.
12. [Tests] Write some tests for the Project Form page